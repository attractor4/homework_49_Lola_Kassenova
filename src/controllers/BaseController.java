package controllers;


import com.sun.net.httpserver.HttpExchange;
import controllers.templates.ErrorController;
import controllers.templates.NotFoundController;
import utils.EndpointMaker;
import utils.Log;

import java.util.Map;

public class BaseController implements Controller
{
    private final Map<String, Controller> routes;

    private final ErrorController errorController = new ErrorController();
    private final NotFoundController notFoundController = new NotFoundController();

    public BaseController(Map<String, Controller> routes)
    {
        this.routes = routes;
    }

    @Override
    public void handle(HttpExchange exchange)
    {
        try
        {
            Log.info("Handle request - '" + exchange.getRequestMethod() + " " + exchange.getRequestURI() + "', from - " + exchange.getRemoteAddress());

            String key = EndpointMaker.make(exchange);
            routes.getOrDefault(key, notFoundController).handle(exchange);
        }
        catch (Exception exception)
        {
            Log.error("Error while handling request - '" + exchange.getRequestURI() + "', message - " + exception.getMessage());
            errorController.handle(exchange);
        }

        exchange.close();
    }
}