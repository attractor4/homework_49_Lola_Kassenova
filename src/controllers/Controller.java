package controllers;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;

public interface Controller
{
    public void handle(HttpExchange exchange) throws IOException;
}
