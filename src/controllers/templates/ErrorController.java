package controllers.templates;

import com.sun.net.httpserver.HttpExchange;
import constants.ContentType;
import constants.StatusCode;
import controllers.Controller;
import servlets.HttpServlet;

public class ErrorController implements Controller
{
    private final HttpServlet handler = new HttpServlet();

    @Override
    public void handle(HttpExchange exchange)
    {
        handler.handle(exchange, ContentType.JSON, StatusCode.INTERNAL_SERVER_ERROR, "Internal server error, try later!".getBytes());
    }
}
