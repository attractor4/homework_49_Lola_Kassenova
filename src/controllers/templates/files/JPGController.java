package controllers.templates.files;

import com.sun.net.httpserver.HttpExchange;
import constants.ContentType;
import controllers.Controller;
import servlets.FileServlet;

import java.io.IOException;

public class JPGController implements Controller
{
    private final FileServlet servlet = new FileServlet();

    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        servlet.handle(exchange, ContentType.JPEG);
    }
}
