package controllers.templates;

import com.sun.net.httpserver.HttpExchange;
import controllers.Controller;
import models.*;
import servlets.TemplateServlet;
import utils.FromParser;
import utils.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserSelectController implements Controller
{
    private final TemplateServlet<SampleDataModel> handler = new TemplateServlet<SampleDataModel>();
    public Map<String, String> queryToMap(String query) {
        if(query == null) {
            return null;
        }
        Map<String, String> result = new HashMap<>();
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                result.put(entry[0], entry[1]);
            }else{
                result.put(entry[0], "");
            }
        }
        return result;
    }
    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        InputStream stream = exchange.getRequestBody();
        InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);

        BufferedReader bufferedReader = new BufferedReader(reader);

        String payload = bufferedReader.lines().collect(Collectors.joining());

        Map<String, String> variables = FromParser.parse(payload, "&");
        if(variables.containsKey("user_id")) {
            exchange.getResponseHeaders().add("Location", "/users/single?id=" + variables.get("user_id"));
            exchange.sendResponseHeaders(303, 0);
            exchange.getResponseBody().close();
        }
    }
}
