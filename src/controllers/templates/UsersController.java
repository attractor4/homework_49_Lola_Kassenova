package controllers.templates;

import com.sun.net.httpserver.HttpExchange;
import controllers.Controller;
import models.SampleDataModel;
import servlets.TemplateServlet;

import java.io.IOException;

public class UsersController implements Controller
{
    private final TemplateServlet<SampleDataModel> handler = new TemplateServlet<SampleDataModel>();

    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        handler.handle(exchange, "users.html", new SampleDataModel());
    }
}
