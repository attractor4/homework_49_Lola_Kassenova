package controllers.templates;

import com.sun.net.httpserver.HttpExchange;
import controllers.Controller;
import models.Book;
import models.SampleDataModel;
import servlets.TemplateServlet;
import utils.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BookController implements Controller
{
    private final TemplateServlet<SampleDataModel> handler = new TemplateServlet<SampleDataModel>();
    public Map<String, String> queryToMap(String query) {
        if(query == null) {
            return null;
        }
        Map<String, String> result = new HashMap<>();
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                result.put(entry[0], entry[1]);
            }else{
                result.put(entry[0], "");
            }
        }
        return result;
    }
    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        Map<String, String> params = queryToMap(exchange.getRequestURI().getQuery());
        SampleDataModel sampleDataModel = new SampleDataModel();
        Book book = sampleDataModel.getBookList().stream().filter(b -> b.getId() == Integer.parseInt(params.get("id"))).findAny().orElse(null);
        if(book != null) {
            sampleDataModel.setSampleBook(book);
        }
        handler.handle(exchange, "book.html", sampleDataModel);
    }
}
