package controllers.templates;

import com.sun.net.httpserver.HttpExchange;
import controllers.Controller;
import models.Book;
import models.SampleDataModel;
import models.User;
import models.UserBook;
import servlets.TemplateServlet;
import utils.Log;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserController implements Controller
{
    private final TemplateServlet<SampleDataModel> handler = new TemplateServlet<SampleDataModel>();
    public Map<String, String> queryToMap(String query) {
        if(query == null) {
            return null;
        }
        Map<String, String> result = new HashMap<>();
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                result.put(entry[0], entry[1]);
            }else{
                result.put(entry[0], "");
            }
        }
        return result;
    }
    @Override
    public void handle(HttpExchange exchange) throws IOException
    {
        Map<String, String> params = queryToMap(exchange.getRequestURI().getQuery());
        SampleDataModel sampleDataModel = new SampleDataModel();
        User user = sampleDataModel.getUserList().stream().filter(u -> u.getId() == Integer.parseInt(params.get("id"))).findAny().orElse(null);
        if(user != null) {
            sampleDataModel.setSampleUser(user);
            List<UserBook> userBookListTaken = sampleDataModel.getUserBookList().stream().filter(u -> u.getUserId() == user.getId() && u.getStatus() == 0).collect(Collectors.toList());
            userBookListTaken.forEach(u -> u.setBook(sampleDataModel.getBookList().stream().filter(b -> b.getId() == u.getBookId()).findFirst().orElse(null)));
            sampleDataModel.setSampleUserBookListTaken(userBookListTaken);
            List<UserBook> userBookListPast = sampleDataModel.getUserBookList().stream().filter(u -> u.getUserId() == user.getId() && u.getStatus() == 1).collect(Collectors.toList());
            userBookListPast.forEach(u -> u.setBook(sampleDataModel.getBookList().stream().filter(b -> b.getId() == u.getBookId()).findFirst().orElse(null)));
            sampleDataModel.setSampleUserBookListPrevious(userBookListPast);
        }
        handler.handle(exchange, "user.html", sampleDataModel);
    }
}
