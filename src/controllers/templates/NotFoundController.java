package controllers.templates;

import com.sun.net.httpserver.HttpExchange;
import controllers.Controller;
import servlets.TemplateServlet;

public class NotFoundController implements Controller
{
    private final TemplateServlet<?> handler = new TemplateServlet<>();

    @Override
    public void handle(HttpExchange exchange)
    {
        handler.handle(exchange, "not_found.html", null);
    }
}
