package models;

import java.time.LocalDateTime;
import java.util.Date;

public class UserBook {
    private int bookId;
    private int userId;
    private LocalDateTime borrowedDatetime;
    private LocalDateTime returnedDatetime;
    private int status;

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    private Book book;

    public UserBook(int bookId, int userId, LocalDateTime borrowedDatetime) {
        this.bookId = bookId;
        this.userId = userId;
        this.borrowedDatetime = borrowedDatetime;
        this.status = 1;
    }

    public UserBook(int bookId, int userId, LocalDateTime borrowedDatetime, int status) {
        this.bookId = bookId;
        this.userId = userId;
        this.borrowedDatetime = borrowedDatetime;
        this.status = status;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public LocalDateTime getBorrowedDatetime() {
        return borrowedDatetime;
    }

    public void setBorrowedDatetime(LocalDateTime borrowedDatetime) {
        this.borrowedDatetime = borrowedDatetime;
    }

    public LocalDateTime getReturnedDatetime() {
        return returnedDatetime;
    }

    public void setReturnedDatetime(LocalDateTime returnedDatetime) {
        this.returnedDatetime = returnedDatetime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
