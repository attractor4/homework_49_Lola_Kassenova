package models;

public class Book {
    private int id;
    private String name;
    private String author;
    private String image;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private String description;
    private int currentStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Book(int id, String name, String author, String image, String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.image = image;
        this.description = description;
        this.currentStatus = 1;
    }

    public Book(int id, String name, String author, String image, int currentStatus, String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.image = image;
        this.description = description;
        this.currentStatus = currentStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(int currentStatus) {
        this.currentStatus = currentStatus;
    }
}
