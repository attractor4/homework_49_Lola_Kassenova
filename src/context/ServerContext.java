package context;

import com.sun.net.httpserver.HttpServer;
import configurations.ServerConfiguration;
import constants.ApplicationConstant;
import controllers.BaseController;
import controllers.Controller;
import utils.Json;
import utils.Log;

import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

public class ServerContext
{
    public HttpServer create() throws Exception
    {
        ServerConfiguration configuration = Json.parseFile(ApplicationConstant.CONFIGURATION_PATH.getValue(), ServerConfiguration.class);

        InetSocketAddress socket = new InetSocketAddress(configuration.getHost(), configuration.getPort());

        HttpServer server = HttpServer.create(socket, configuration.getBacklog());
        Log.info("Server successfully started on this address - http://" + configuration.getHost() + ":" + configuration.getPort() + "/");

        initRoutes(server, configuration);

        return server;
    }

    private void initRoutes(HttpServer server, ServerConfiguration configuration)
    {
        Map<String, Controller> routes = new HashMap<>();

        configuration.getRoutes().forEach((key, value) ->
        {
            try
            {
                routes.put(key, (Controller) Class.forName(value).newInstance());
            }
            catch (InstantiationException | IllegalAccessException | ClassNotFoundException e)
            {
                Log.error("Error while getting configs, message - " + e.getMessage());
                System.exit(1);
            }
        });

        server.createContext("/", new BaseController(routes)::handle);
    }
}
