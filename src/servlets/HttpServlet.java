package servlets;

import com.sun.net.httpserver.HttpExchange;
import constants.ContentType;
import constants.StatusCode;
import utils.Log;

import java.io.OutputStream;


public class HttpServlet
{
    public void handle(HttpExchange exchange, ContentType type, StatusCode code, byte[] payload)
    {
        try(OutputStream stream = exchange.getResponseBody())
        {
            exchange.getResponseHeaders().add("Content-Type", type.getType());
            exchange.getResponseHeaders().add("Content-Length", String.valueOf(payload.length));

            exchange.sendResponseHeaders(code.getCode(), payload.length);

            stream.write(payload);

            stream.close();
            stream.flush();

            exchange.close();
        }
        catch (Exception exception)
        {
            Log.error("Servlet get exception while handle request: " + exception.getMessage());
        }
    }
}
