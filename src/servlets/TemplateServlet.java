package servlets;

import com.sun.net.httpserver.HttpExchange;
import configurations.FreeMarkerConfiguration;
import constants.ContentType;
import constants.StatusCode;
import freemarker.template.Configuration;
import freemarker.template.Template;
import utils.Log;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;


public class TemplateServlet<TEntity>
{
    private final HttpServlet handler = new HttpServlet();
    private final Configuration configuration = FreeMarkerConfiguration.build();

    public void handle(HttpExchange exchange, String file, TEntity tEntity)
    {
        try
        {
            Template template = configuration.getTemplate(file);

            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            try (OutputStreamWriter writer = new OutputStreamWriter(stream))
            {
                template.process(tEntity, writer);
                writer.flush();

                byte[] data = stream.toByteArray();
                handler.handle(exchange, ContentType.HTML, StatusCode.SUCCESSFUL, data);
            }
        }
        catch (Exception exception)
        {
            Log.error("Servlet get exception while handle request: " + exception.getMessage());
        }
    }
}
