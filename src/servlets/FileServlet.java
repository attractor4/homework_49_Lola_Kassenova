package servlets;

import com.sun.net.httpserver.HttpExchange;
import constants.ApplicationConstant;
import constants.ContentType;
import constants.StatusCode;
import controllers.templates.NotFoundController;
import utils.Log;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class FileServlet
{
    private final HttpServlet handler = new HttpServlet();

    public void handle(HttpExchange exchange, ContentType type) throws IOException
    {
        try {

            Path path = Paths.get(ApplicationConstant.TEMPLATES_PATH.getValue(), exchange.getRequestURI().getPath());

            if (Files.notExists(path))
            {
                new NotFoundController().handle(exchange);
            }

            byte[] bytes = Files.readAllBytes(path);
            handler.handle(exchange, type, StatusCode.SUCCESSFUL, bytes);
        }
        catch (Exception exception)
        {
            Log.error("Servlet get exception while handle request: " + exception.getMessage());
        }
    }
}
