package utils;

import constants.ApplicationConstant;

import java.io.FileWriter;
import java.io.IOException;

public class Log
{
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";

    public static void info(String string)
    {
        String log = String.format("| INFO  - %s\n", string);
        System.out.print(ANSI_GREEN + log + ANSI_RESET);

        log(log);
    }

    public static void error(String string)
    {
        String log = String.format("| ERROR - %s\n", string);
        System.out.print(ANSI_RED + log + ANSI_RESET);

        log(log);
    }

    public static void warn(String string)
    {
        String log = String.format("| WARN  - %s\n", string);
        System.out.print(ANSI_YELLOW + log + ANSI_RESET);

        log(log);
    }


    private static void log(String log)
    {
        try(FileWriter writer = new FileWriter(ApplicationConstant.LOG_FILE_PATH.getValue(), true))
        {
            writer.write(log);
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
