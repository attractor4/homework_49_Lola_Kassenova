package utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.FileReader;
import java.io.IOException;

public class Json
{
    private static final Gson gson = new GsonBuilder().create();

    public static <TEntity> TEntity parseJson(String json, Class<TEntity> type)
    {
        return gson.fromJson(json, type);
    }

    public static <TEntity> TEntity parseFile(String path, Class<TEntity> type) throws Exception
    {
        try(FileReader reader = new FileReader(path))
        {
            return gson.fromJson(reader, type);
        }
        catch (IOException exception)
        {
            Log.error("File with path \"" + path + "\" not found!");
        }

        throw new Exception("Parsing exception error");
    }
}
