package utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FromParser {
    public static Map<String, String> parse(String payload, String delimiter) {
        String[] variables = payload.split(delimiter);
        Stream<Map.Entry<String, String>> stream = Arrays.stream(variables)
                .map(FromParser::decode)
                .filter(Optional::isPresent)
                .map(Optional::get);
        return stream.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
    }
    private static Optional<Map.Entry<String, String>> decode(String variable) {
        if(!variable.contains("=")) {
            return Optional.empty();
        }

        String[] pair = variable.split("=");

        if(pair.length != 2) {
            return Optional.empty();
        }
        String charset = StandardCharsets.UTF_8.name();
        try {
            String key = URLDecoder.decode(pair[0], charset);
            String value = URLDecoder.decode(pair[1], charset);
            return Optional.of(new AbstractMap.SimpleEntry<>(key, value));
        } catch (UnsupportedEncodingException e) {
            Log.error(e.toString());
        }
        return Optional.empty();
    }
}
