package constants;

public enum ApplicationConstant
{
    LOG_FILE_PATH("resources/logs.txt"),
    TEMPLATES_PATH("resources/templates"),
    CONFIGURATION_PATH("resources/configurations/application.json");

    private final String value;

    ApplicationConstant(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
