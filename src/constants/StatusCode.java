package constants;

public enum StatusCode
{
    SUCCESSFUL(200),
    NOT_FOUND(404),
    NOT_IMPLEMENTED(501),
    INTERNAL_SERVER_ERROR(500);

    private final Integer code;

    StatusCode(Integer code)
    {
        this.code = code;
    }

    public Integer getCode()
    {
        return this.code;
    }
}
