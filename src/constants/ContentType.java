package constants;

public enum ContentType
{
    HTML("text/html; charset=utf-8"),
    JSON("application/json; charset=utf-8"),
    PLAIN("text/plain; charset=utf-8"),

    CSS("text/css"),
    JPEG("image/jpeg"),
    PNG("image/png"),
    GIF("image/gif");

    private final String type;

    ContentType(String type)
    {
        this.type = type;
    }

    public String getType()
    {
        return this.type;
    }
}
