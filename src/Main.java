import com.sun.net.httpserver.HttpServer;
import context.ServerContext;
import utils.Log;

import java.util.HashMap;
import java.util.Map;

public class Main
{
    public static void main(String[] args)
    {
        try
        {
            ServerContext context = new ServerContext();

            HttpServer server = context.create();
            server.start();
        }
        catch (Exception exception)
        {
            Log.error("Server doesn't started, message - " + exception.getMessage() + "...");
            System.exit(1);
        }
    }
}