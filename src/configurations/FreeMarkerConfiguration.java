package configurations;

import constants.ApplicationConstant;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import java.io.File;
import java.io.IOException;

public class FreeMarkerConfiguration
{
    public static Configuration build()
    {
        try
        {
            Configuration configuration = new Configuration(Configuration.VERSION_2_3_29);

            configuration.setDirectoryForTemplateLoading(new File(ApplicationConstant.TEMPLATES_PATH.getValue()));
            configuration.setDefaultEncoding("UTF-8");

            configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            configuration.setLogTemplateExceptions(false);
            configuration.setWrapUncheckedExceptions(true);
            configuration.setFallbackOnNullLoopVariable(false);

            return configuration;
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
