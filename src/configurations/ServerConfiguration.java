package configurations;

import java.util.Map;

public class ServerConfiguration
{
    private String host;
    private Integer port;

    private Integer backlog;

    Map<String, String> routes;


    public String getHost()
    {
        return host;
    }

    public Integer getPort()
    {
        return port;
    }

    public Integer getBacklog()
    {
        return backlog;
    }

    public Map<String, String > getRoutes()
    {
        return routes;
    }
}

